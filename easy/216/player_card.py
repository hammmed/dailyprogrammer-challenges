import unittest

class Player_Card(Card):
	"""Player Card objecct, extends Card to store information about the owner
	of the card
	"""
	def __init__(self, player_name):
		self.player_name = player_name

class Player_Card_Test(unittest.TestCase):
	"""Unit tests for the Player_Card class"""
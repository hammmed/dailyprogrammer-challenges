#!/usr/bin/env python

import deck
import player
import poker_engine

d 		= deck.Deck()
p 		= player.Player("Mo")
engine 	= poker_engine.Poker_Engine(3)

# d.shuffle_deck()
# p.add_to_hand(d.deal_card().name)
# print p.hand

engine.deal_hand(2)
engine.print_hands()

# for player in engine.players:
# 	for card in player.hand:
# 		print card

engine.deal_flop()
engine.deal_turn()
engine.deal_river()

for card in engine.table_cards:
	print card
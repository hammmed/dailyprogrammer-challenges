/r/dailyprogrammer Easy challenge #216

Texas Hold 'Em Poker

Input:
	- Number of players (2-8)

Output:
	- Each card dealt to every player
	- Community cards
	- Burn pile (optional)

Sample Output:

	How many players (2-8) ? 3

	Your hand: 2 of Clubs, 5 of Diamonds
	CPU 1 Hand: Ace of Spades, Ace of Hearts
	CPU 2 Hand: King of Clubs, Queen of Clubs

	Flop: 2 of Hearts, 5 of Clubs, Ace of Clubs
	Turn: King of Hearts
	River: Jack of Hearts
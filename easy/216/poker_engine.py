import unittest

import deck
import player

class Poker_Engine(object):
	def __init__(self, num_players):
		"""Poker Game Engine"""
		self.deck 			= deck.Deck()
		self.players 		= []
		self.table_cards 	= []

		for x in range(num_players):
			pname = "Player %d" % (x + 1)
			self.players.append(player.Player(pname))

	def deal_round(self):
		"""Method to deal a card to every player"""
		for player in self.players:
			player.add_to_hand(self.deck.deal_card())

	def deal_hand(self, cards):
		"""Deals the desired amount of cards to all players"""
		self.deck.shuffle_deck()

		for x in range(cards):
			self.deal_round()

	def deal_flop(self):
		"""Deal flop"""
		self.deck.deal_card()
		self.table_cards = self.deck.deal_cards(3)

	def deal_turn(self):
		"""Deal turn"""
		self.deck.deal_card()
		self.table_cards.append(self.deck.deal_card())

	def deal_river(self):
		"""Deal river"""
		self.deck.deal_card()
		self.table_cards.append(self.deck.deal_card())

	def print_hands(self):
		"""Prints he hands of all players
		"""
		player_string = ""

		for player in self.players:
			player_string = ""
			player_string += "%s's hand: " % player.name
			cards = 0
			for card in player.hand:
				if cards == 1:
					player_string += ", "

				player_string += "%s" % card
				cards += 1

			print player_string

		print ""

class Test_Poker_Engine(unittest.TestCase):
	"""Unit tests for Poker Engine class"""

	# construction test
	#	- check number is passed in
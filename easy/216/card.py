import unittest

class Card(object):
	"""Card object that stores suits and value for a card
	"""
	values = ["Ace", "2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack",
		"Queen", "King"]
	suits = ["Spades", "Clubs", "Hearts", "Diamonds"]

	def __init__(self, suit, value):
		self.value = value
		self.suit = suit
		self.name = "%s of %s" % (value, suit)

	def __str__(self):
		return "%s of %s" % (self.value, self.suit)

class Card_Test(unittest.TestCase):
	"""Unit tests for the Card class"""
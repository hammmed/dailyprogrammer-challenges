import unittest

class Table_Card(Card):
	"""Table_Card objecct, extends Card to store information about the owner
	of the card
	"""
	def __init__(self, position):
		self.position = position

class Table_Card_Test(unittest.TestCase):
	"""Unit tests for the Table_Card class"""
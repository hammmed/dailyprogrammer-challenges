import card
import unittest

class Player(object):
	def __init__(self, name):
		self.name = name
		self.hand = []

	def add_to_hand(self, card):
		"""Check whether a card can be added, and then do it
		"""
		self.hand.append(card)

class Player_Test(unittest.TestCase):
	"""Unit tests for the Player class"""

	def setUp(self):
		self.p = Player("test")

	# add_to_hand_tests
	#	- check invalid card (not Card object)
	# 	- check invalid hand size
import unittest
import random

from card import Card

class Deck(object):
	"""Deck object
	"""
	def __init__(self):
		self.deck = [Card(suit, value) for suit in Card.suits for value in
			Card.values]

		# card_key = 0

		# for suit in Card.suits:
		# 	for value in Card.values:

	def shuffle_deck(self):
		"""Shuffle the cards in the deck"""
		old_deck 	= self.deck
		random.shuffle(old_deck)
		self.deck = old_deck

	def deal_card(self):
		"""Deals a single card, removes and returns it"""
		return self.deck.pop(0)

	def deal_cards(self, num_cards):
		"""Deals x amount of cards"""
		cards = []
		for x in range(num_cards):
			cards.append(self.deal_card())

		return cards

class Deck_Test(unittest.TestCase):
	"""Unit tests for the Deck class"""

	def setUp(self):
		self.d = Deck()

	# Left this out because I'm not entirely sure how to test for this

	def test_shuffle_deck_shuffles(self):
		"""Checks that the deck order has changed after shuffling"""
		old_deck = self.d.deck

		self.d.shuffle_deck()
		
		self.assertFalse(self.d.deck == old_deck)

	def test_card_dealt(self):
		"""Check card returned"""

	def test_dealt_removed(self):
		"""Check deck size decremented"""

if __name__ == '__main__':
	unittest.main()